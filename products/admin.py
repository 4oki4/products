from django.contrib import admin
from .models import Foodstuff

admin.site.register(Foodstuff)
