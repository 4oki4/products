from django.db import models
from django.urls import reverse


class Foodstuff(models.Model):
    id = primary_key = True
    name = models.CharField(max_length=100)
    price = models.PositiveIntegerField()
    amount = models.PositiveIntegerField()
    comment = models.TextField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('prod_detail', args=[str(self.id)])
