from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Foodstuff
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView


class FoodListView(ListView):
    model = Foodstuff
    template_name = 'food.html'
    context_object_name = "foods"


class FoodDetailView(DetailView):
    model = Foodstuff
    template_name = 'prod_detail.html'


class FoodCreateView(CreateView):
    model = Foodstuff
    template_name = 'prod_new.html'
    fields = '__all__'


class FoodUpdateView(UpdateView):
    model = Foodstuff
    template_name = 'prod_edit.html'
    fields = ['name', 'price', 'amount', 'commend']


class FoodDeleteView(DeleteView):
    model = Foodstuff
    template_name = 'prod_delete.html'
    success_url = reverse_lazy('food')

