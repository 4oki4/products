from .views import FoodListView, FoodDetailView, FoodCreateView, FoodUpdateView, FoodDeleteView
from django.urls import path

urlpatterns = [
    path('', FoodListView.as_view(), name='food'),
    path('food/<int:pk>/', FoodDetailView.as_view(), name='prod_detail'),
    path('food/new/', FoodCreateView.as_view(), name='prod_new'),
    path('food/<int:pk>/edit', FoodUpdateView.as_view(), name='prod_edit'),
    path('food/<int:pk>/delete', FoodDeleteView.as_view(), name='prod_delete'),
]

